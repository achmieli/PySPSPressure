import pytimber
from pytimber import pagestore
import time, calendar

ldb = pytimber.LoggingDB()

mydbPressureMDB = pagestore.PageStore('spsPressure.db', './../../SPS_PRESSURE')
#mydbPressure = pagestore.PageStore('mydataPressure.db', './../../SPS_Pressure_data')

t1 = "2017-06-30 23:00:00"
#t1b1 = "2017-06-29 18:00:00"
t2 = "2017-06-30 23:59:59"
# t1 = '2017-10-11 00:00:00' #(input("Start date? (ex.2017-07-25 10:15:00.000)\n"))
# t2 = '2017-10-11 06:00:00' #(input("End date? (ex.2017-07-25 10:15:00.000)\n"))

ts1 = calendar.timegm(time.strptime(t1,"%Y-%m-%d %H:%M:%S")) - 2 * 3600
ts2 = calendar.timegm(time.strptime(t2,"%Y-%m-%d %H:%M:%S")) - 2 * 3600

keys_beam1 = ['MKI.D5L2.B1:PRESSURE', 'MKI.B5L2.B1:PRESSURE', 'MKI.C5L2.B1:PRESSURE', 'MKI.A5L2.B1:PRESSURE', 'VGPB.137.5L2.B.PR', 'VGPB.14.5L2.B.PR', 'VGPB.176.5L2.B.PR', 'VGPB.59.5L2.B.PR', 'VGPB.98.5L2.B.PR', 'LHC.BCTFR.A6R4.B1:BEAM_INTENSITY', 'MKI.A5L2.B1:PRESSURE_INT', 'MKI.B5L2.B1:PRESSURE_INT', 'MKI.C5L2.B1:PRESSURE_INT', 'MKI.D5L2.B1:PRESSURE_INT']
keys_beam2 = ['MKI.B5R8.B2:PRESSURE', 'MKI.A5R8.B2:PRESSURE', 'MKI.C5R8.B2:PRESSURE', 'MKI.D5R8.B2:PRESSURE', 'VGPB.138.5R8.R.PR', 'VGPB.14.5R8.R.PR', 'VGPB.176.5R8.R.PR', 'VGPB.59.5R8.R.PR', 'VGPB.98.5R8.R.PR', 'LHC.BCTFR.A6R4.B2:BEAM_INTENSITY', 'MKI.A5R8.B2:PRESSURE_INT', 'MKI.B5R8.B2:PRESSURE_INT', 'MKI.C5R8.B2:PRESSURE_INT', 'MKI.D5R8.B2:PRESSURE_INT']


def Store_data(keys):
    timber_data = ldb.getAligned(keys, ts1, ts2)
    for key in keys:
        mydbPressureMDB.store_variable(key, timber_data['timestamps'], timber_data[key]);

Store_data(keys_beam1+keys_beam2)