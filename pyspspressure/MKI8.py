import matplotlib.pyplot as plt
import pytimber
import time, calendar
from pytimber import pagestore


# DEFINING FUNCTIONS

# sorting by the length of the array
def sort(x):
    return sorted(x, key=lambda time: time[1], reverse=True)


# choosing second magnet(in array length) for alignment
def get_magnet(x):
    return x[1]


# calculating elapsed time
elap = []


def elapsed(ts, ts0):
    return (ts - ts0) / 3600

# new script for beam elapsed time
beam_elap = []



def beam_elapsed(tsOne, tsZero, bi, counti):
    global beam_elap_running_total
    #print(counti)
    if bi >= 10 ** 12 and counti>0:
        beam_elap_running_total=beam_elap_running_total+(tsOne - tsZero) / 3600
        #print(beam_elap_running_total, tsOne, tsZero, counti)
        return beam_elap_running_total
    
    else: 
        return beam_elap_running_total+10 ** (-12)

# calculating normalized pressure
normA = []
normB = []
normC = []
normD = []
normDC = []
normAQ4 = []
normQ5D = []
normBA = []
normCB = []


def normalization(p, bi):
    if bi >= 10 ** 12:
        return p / bi
    else:
        return 10 ** -99

# RUNNING THE CODE
#NOTE: for MKI8: 16/04/2018 to 02/05/2018 => 1,382,029 entries = 59,586s (16.5hrs) aligning data

# defining the time window
t1 = "2018-05-11 00:00:00"
t2 = "2018-05-11 01:59:59"
ts1 = calendar.timegm(time.strptime(t1,"%Y-%m-%d %H:%M:%S")) - 2 * 3600
ts2 = calendar.timegm(time.strptime(t2,"%Y-%m-%d %H:%M:%S")) - 2 * 3600

beam_elap_running_total=0
t0_beam_elap_running_total_start_counter = "2018-04-15 00:00:00"


t0=beam_elap_running_total

text1="Start time = ",t1," End time = ",t2," Start beam elapsed time counter = ",int(t0*10)/10,"hrs, since:",t0_beam_elap_running_total_start_counter
print(text1)

# changing the 'master' data to the first position in the array
print("Reading data for magnets")
parameters0 = ["MKI.A5R8.B2:PRESSURE", "MKI.B5R8.B2:PRESSURE", "MKI.C5R8.B2:PRESSURE", "MKI.D5R8.B2:PRESSURE"]
parameters = parameters0 + ["VGPB.138.5R8.R.PR", "VGPB.14.5R8.R.PR", "VGPB.176.5R8.R.PR", "VGPB.59.5R8.R.PR",
                            "VGPB.98.5R8.R.PR", "LHC.BCTFR.A6R4.B2:BEAM_INTENSITY", "MKI.A5R8.B2:PRESSURE_INT", 
                            "MKI.B5R8.B2:PRESSURE_INT", "MKI.C5R8.B2:PRESSURE_INT", "MKI.D5R8.B2:PRESSURE_INT"]
#parameters = parameters0 + ("VGPB.138.5R8.R.PR", "VGPB.176.5R8.R.PR", 
#                            "LHC.BCTFR.A6R4.B2:BEAM_INTENSITY")
print(parameters)
start_time = time.time()
pageStore = pagestore.PageStore('spsPressure.db', './../../SPS_PRESSURE')
data = pageStore.get(parameters, ts1, ts2)
reading_aligned_time = time.time()-start_time
print("Execution time: aligning data: %0.3f seconds." %reading_aligned_time)

# the dictionary returned contains one list of timestamps and one entry per variable with a list of values.
# all parameters are aligned with the first one

# separating time data from pressure/intensity data
aligned_time = data['MKI.A5R8.B2:PRESSURE'][0]
pressureA = data['MKI.A5R8.B2:PRESSURE'][1]
pressureB = data['MKI.B5R8.B2:PRESSURE'][1]
pressureC = data['MKI.C5R8.B2:PRESSURE'][1]
pressureD = data['MKI.D5R8.B2:PRESSURE'][1]
pressureDC = data['VGPB.138.5R8.R.PR'][1]
pressureAQ4 = data['VGPB.14.5R8.R.PR'][1]
pressureQ5D = data['VGPB.176.5R8.R.PR'][1]
pressureBA = data['VGPB.59.5R8.R.PR'][1]
pressureCB = data['VGPB.98.5R8.R.PR'][1]
intensity = data['LHC.BCTFR.A6R4.B2:BEAM_INTENSITY'][1]
pressureA_int = data['MKI.A5R8.B2:PRESSURE_INT'][1]
pressureB_int = data['MKI.B5R8.B2:PRESSURE_INT'][1]
pressureC_int = data['MKI.C5R8.B2:PRESSURE_INT'][1]
pressureD_int = data['MKI.D5R8.B2:PRESSURE_INT'][1]

print("*** Number of entries for alinged_time = ", len(aligned_time))

# calculating elapsed time, normalized pressure and beam elapsed time and pressure/normalized pressure
start_time = time.time()
for i in range(len(aligned_time)):
    elap.append(elapsed(aligned_time[i], aligned_time[0]))
    beam_elap.append(beam_elapsed(aligned_time[i], aligned_time[i-1], intensity[i], i))    
    normA.append(normalization(pressureA[i], intensity[i]))
    normB.append(normalization(pressureB[i], intensity[i]))
    normC.append(normalization(pressureC[i], intensity[i]))
    normD.append(normalization(pressureD[i], intensity[i]))
    normDC.append(normalization(pressureDC[i], intensity[i]))
    normAQ4.append(normalization(pressureAQ4[i], intensity[i]))
    normQ5D.append(normalization(pressureQ5D[i], intensity[i]))
    normBA.append(normalization(pressureBA[i], intensity[i]))
    normCB.append(normalization(pressureCB[i], intensity[i]))
calculation_time = time.time()-start_time
print("Execution time: calculating normalized data and elapsed time: %0.3f seconds." % calculation_time)

print("Running total of beam elapsed time = ", beam_elap_running_total)
titletext="Start time = ",t1," End time = ",t2," Start/End beam elapsed time counter = ",int(t0*10)/10,"/",int(10*beam_elap_running_total)/10,"hrs. Since:",t0_beam_elap_running_total_start_counter

# PLOTTING DATA
# Plot 1, FIGURE 1
print("Plot 1, Figs. 1-4")
start_time = time.time()
fig, axarr = plt.subplots(2, 2, figsize=(18, 9))


# PLOT normalized pressure versus elapsed time
#axarr[0, 0].semilogy()
#axarr[0, 0].set_ylim(10 ** -25, 10 ** -21)
#axarr[0, 0].set_ylabel(r'Normalized pressure (mbar/p)')
#axarr[0, 0].plot(elap, normA, '-b', label='MKI8A')
#axarr[0, 0].plot(elap, normB, '-r', label='MKI8B')
#axarr[0, 0].plot(elap, normC, '-c', label='MKI8C')
#axarr[0, 0].plot(elap, normD, '-m', label='MKI8D')
#axarr[0, 0].plot(elap, normDC, '-y', label='Interconnect MKI8C-MKI8D')
#axarr[0, 0].plot(elap, normAQ4, '-k', label='Interconnect MKI8A-Q4')
#axarr[0, 0].plot(elap, normQ5D, 'tab:grey', label='Interconnect MKI8D-Q5')
#axarr[0, 0].plot(elap, normBA, 'tab:orange', label='Interconnect MKI8A-MKI8B')
#axarr[0, 0].plot(elap, normCB, 'tab:brown', label='Interconnect MKI8B-MKI8C')
#axarr[0, 0].set_xlabel(r'Elapsed time')
#axarr[0, 0].legend()
#axarr[0, 0].legend(ncol=3, bbox_to_anchor=(1.5, 1.3))

# normalized pressure versus elapsed beam time

#fig.suptitle(MKI8)
axarr[0, 0].semilogy()
#axarr[0, 0].set_ylim(10 ** -25, 2*10 ** -20)
axarr[0, 0].set_ylabel(r'Normalized pressure (mbar/p)')
axarr[0, 0].set_ylim(10 ** -25, 2*10 ** -22)
#axarr[0, 0].plot(beam_elap, normA, '-b', label='MKI8A')
#axarr[0, 0].plot(beam_elap, normB, '-r', label='MKI8B')
#axarr[0, 0].plot(beam_elap, normC, '-k', label='MKI8C')
axarr[0, 0].plot(beam_elap, normD, '-m', label='MKI8D')
axarr[0, 0].plot(beam_elap, normDC, 'tab:orange', label='Interconnect MKI8C-MKI8D')
#axarr[0, 0].plot(beam_elap, normAQ4beam, '-k', label='Interconnect MKI8A-Q4')
axarr[0, 0].plot(beam_elap, normQ5D, 'tab:grey', label='Interconnect MKI8D-Q5')
#axarr[0, 0].plot(beam_elap, normBAbeam, 'tab:orange', label='Interconnect MKI8A-MKI8B')
#axarr[0, 0].plot(beam_elap, normCBbeam, 'tab:brown', label='Interconnect MKI8B-MKI8C')
axarr[0, 0].set_xlabel(r'Elapsed beam time [>10^12 p] (hrs)')
axarr[0, 0].legend()
axarr[0, 0].legend(ncol=3, bbox_to_anchor=(1.5, 1.17))


# Plot 1, FIGURE 2
# pressure versus elapsed time
#print("Fig. 2")
axarr[0, 1].set_ylabel(r'Pressure (mbar)')
ax0 = axarr[0, 1].twinx()
ax0.set_ylabel(r'Beam intensity (p)')
ax0.tick_params(axis='y', colors='green')
ax0.yaxis.label.set_color('green')

#axarr[0, 1].plot(elap, pressureA, '-y', label='MKI8A')
#axarr[0, 1].plot(elap, pressureB, '-r', label='MKI8B')
#axarr[0, 1].plot(elap, pressureC, '-c', label='MKI8C')
axarr[0, 1].plot(elap, pressureD, '-m', label='MKI8D')
axarr[0, 1].plot(elap, pressureDC, '-b', label='Interconnect MKI8C-MKI8D')
#axarr[0, 1].plot(elap, pressureAQ4, '-k', label='Interconnect MKI8A-Q4')
axarr[0, 1].plot(elap, pressureQ5D, 'tab:grey', label='Interconnect MKI8D-Q5')
#axarr[0, 1].plot(elap, pressureBA, 'tab:orange', label='Interconnect MKI8A-MKI8B')
#axarr[0, 1].plot(elap, pressureCB, 'tab:brown', label='Interconnect MKI8B-MKI8C')
ax0.plot(elap, intensity, '-g', label='Beam Intensity')
axarr[0, 1].set_xlabel(r'Elapsed time (hrs)')

# FIGURE 3
#pressure versus elapsed beam time
#print("Plot 1, Fig. 3")
axarr[0, 1].set_ylabel(r'Pressure (mbar)')
#ax0 = axarr[0, 1].twinx()
#ax0.set_ylabel(r'Beam intensity (p)')
#ax0.tick_params(axis='y', colors='green')
#ax0.yaxis.label.set_color('green')

#axarr[0, 1].plot(beam_elap, pressureAbeam, '-b', label='MKI8A')
#axarr[0, 1].plot(beam_elap, pressureBbeam, '-r', label='MKI8B')
#axarr[0, 1].plot(beam_elap, pressureCbeam, '-c', label='MKI8C')
#axarr[0, 1].plot(beam_elap, pressureDbeam, '-m', label='MKI8D')
#axarr[0, 1].plot(beam_elap, pressureDCbeam, '-y', label='Interconnect MKI8C-MKI8D')
#axarr[0, 1].plot(beam_elap, pressureAQ4beam, '-k', label='Interconnect MKI8A-Q4')
#axarr[0, 1].plot(beam_elap, pressureQ5Dbeam, 'tab:grey', label='Interconnect MKI8D-Q5')
#axarr[0, 1].plot(beam_elap, pressureBAbeam, 'tab:orange', label='Interconnect MKI8A-MKI8B')
#axarr[0, 1].plot(beam_elap, pressureCBbeam, 'tab:brown', label='Interconnect MKI8B-MKI8C')
#ax0.plot(beam_elap, beam, '-g', label='Beam Intensity')
#axarr[0, 1].set_xlabel(r'Elapsed beam time')

# normalized pressure versus UTC time

axarr[1, 0].semilogy()
#axarr[1, 0].set_ylim(10 ** -25, 2*10 ** -20)
axarr[1, 0].set_ylim(10 ** -25, 2*10 ** -22)
axarr[1, 0].set_ylabel(r'Normalized pressure (mbar/p)')
#axarr[1, 0].plot(aligned_time, normA, '-b', label='MKI8A')
#axarr[1, 0].plot(aligned_time, normB, '-r', label='MKI8B')
#axarr[1, 0].plot(aligned_time, normC, '-c', label='MKI8C')
axarr[1, 0].plot(aligned_time, normD, '-m', label='MKI8D')
axarr[1, 0].plot(aligned_time, normDC, '-y', label='Interconnect MKI8C-MKI8D')
#axarr[1, 0].plot(aligned_time, normAQ4, '-k', label='Interconnect MKI8A-Q4')
axarr[1, 0].plot(aligned_time, normQ5D, 'tab:grey', label='Interconnect MKI8D-Q5')
#axarr[1, 0].plot(aligned_time, normBA, 'tab:orange', label='Interconnect MKI8A-MKI8B')
#axarr[1, 0].plot(aligned_time, normCB, 'tab:brown', label='Interconnect MKI8B-MKI8C')
axarr[1, 0].set_xlabel(r'Date & (UTC) time')
pytimber.set_xaxis_date(axarr[1, 0], bins=7)


# Plot 1, FIGURE 4
# pressure versus UTC time
#print("Plot 1, Fig. 4")
axarr[1, 1].set_ylabel(r'Pressure (mbar)')
ax1 = axarr[1, 1].twinx()
ax1.set_ylabel(r'Beam intensity (p)')
ax1.tick_params(axis='y', colors='green')
ax1.yaxis.label.set_color('green')

#axarr[1, 1].plot(aligned_time, pressureA, '-y', label='MKI8A')
#axarr[1, 1].plot(aligned_time, pressureB, '-r', label='MKI8B')
#axarr[1, 1].plot(aligned_time, pressureC, '-c', label='MKI8C')
axarr[1, 1].plot(aligned_time, pressureD, '-m', label='MKI8D')
axarr[1, 1].plot(aligned_time, pressureDC, '-b', label='Interconnect MKI8C-MKI8D')
#axarr[1, 1].plot(aligned_time, pressureAQ4, '-k', label='Interconnect MKI8A-Q4')
axarr[1, 1].plot(aligned_time, pressureQ5D, 'tab:grey', label='Interconnect MKI8D-Q5')
#axarr[1, 1].plot(aligned_time, pressureBA, 'tab:orange', label='Interconnect MKI8A-MKI8B')
#axarr[1, 1].plot(aligned_time, pressureCB, 'tab:brown', label='Interconnect MKI8B-MKI8C')
ax1.plot(aligned_time, intensity, '-g', label='Beam Intensity')
axarr[1, 1].set_xlabel(r'Date & (UTC) time')
pytimber.set_xaxis_date(axarr[1, 1], bins=7)

# plotting limits
#limitMagnet = 2 * 10 ** -9
#limit = []
#for i in range(len(aligned_time)):
    #limit.append(limitMagnet)
#for i in range(len(aligned_time)):
    #if pressureA[i] or pressureB[i] or pressureC[i] or pressureD[i] >= limit[i]:
        #axarr[0, 1].plot(elap, limit, 'tab:pink', label='pressure limit for magnets')
        #axarr[1, 1].plot(aligned_time, limit, 'tab:pink', label='pressure limit for magnets')

#limitInterlock = 5 * 10 ** -8
#limit1 = []
#for i in range(len(aligned_time)):
    #limit1.append(limitInterlock)
#for i in range(len(aligned_time)):
    #if pressureDC[i] or pressureAQ4[i] or pressureQ5D[i] or pressureBA[i] or pressureCB[i] >= limit1[i]:
        #axarr[0, 1].plot(elap, limit1, 'tab:purple', label='pressure limit for interlocks')
        #axarr[1, 1].plot(aligned_time, limit1, 'tab:purple')

# PLOT 1, Fig 0 
fig.suptitle(titletext)
plt.subplots_adjust(left=0.1, right=0.9, bottom=0.1, top=0.9)
plotting_time = time.time()-start_time
print("Execution time: plotting %0.3f seconds." % plotting_time)
plt.show()

axarr[0, 0].semilogy()
#axarr[0, 0].set_ylim(10 ** -25, 2*10 ** -20)
axarr[0, 0].set_ylim(10 ** -25, 2*10 ** -22)
axarr[0, 0].set_ylabel(r'Normalized pressure (mbar/p)')
#axarr[0, 0].plot(beam_elap, normAbeam, '-b', label='MKI8A')
#axarr[0, 0].plot(beam_elap, normBbeam, '-r', label='MKI8B')
#axarr[0, 0].plot(beam_elap, normCbeam, '-c', label='MKI8C')
axarr[0, 0].plot(beam_elap, normD, '-m', label='MKI8D')
axarr[0, 0].plot(beam_elap, normDC, '-y', label='Interconnect MKI8C-MKI8D')
#axarr[0, 0].plot(beam_elap, normAQ4beam, '-k', label='Interconnect MKI8A-Q4')
axarr[0, 0].plot(beam_elap, normQ5D, 'tab:grey', label='Interconnect MKI8D-Q5')
#axarr[0, 0].plot(beam_elap, normBAbeam, 'tab:orange', label='Interconnect MKI8A-MKI8B')
#axarr[0, 0].plot(beam_elap, normCBbeam, 'tab:brown', label='Interconnect MKI8B-MKI8C')
axarr[0, 0].set_xlabel(r'Elapsed beam time [>10^12 p] (hrs)')
axarr[0, 0].legend()
axarr[0, 0].legend(ncol=3, bbox_to_anchor=(1.5, 1.17))

# PLOT 2 *** versus beam elapsed time
print("Plot 2")
plt.figure(figsize=(12,6))
plt.suptitle(titletext)
plt.plot(beam_elap, normA, 'o-', color='red', markersize=2,  label='MKI8A')
plt.plot(beam_elap, normB, 'o-', color='blue', markersize=2,  label='MKI8B')
plt.plot(beam_elap, normC, 'o-', color='purple', markersize=2,  label='MKI8C')
plt.plot(beam_elap, normD, 'o-', color='green', markersize=2,  label='MKI8D')
plt.plot(beam_elap, normDC, 'o-', color='black', markersize=2,  label='Interconnect MKI8C-MKI8D')
#axarr[0, 0].plot(beam_elap, normAQ4beam, '-k', label='Interconnect MKI8A-Q4')
plt.plot(beam_elap, normQ5D, 'tab:grey', markersize=2, label='Interconnect MKI8D-Q5')
#axarr[0, 0].plot(beam_elap, normBAbeam, 'tab:orange', label='Interconnect MKI8A-MKI8B')
#axarr[0, 0].plot(beam_elap, normCBbeam, 'tab:brown', label='Interconnect MKI8B-MKI8C')

ax=plt.gca()
ax.set_ylabel(r'Normalized pressure (mbar/p)')
ax.semilogy()
#ax.set_ylim(10 ** -25, 2*10 ** -20)
ax.set_ylim(10 ** -25, 2*10 ** -22)
ax.set_xlabel(r'Elapsed beam time [>10^12 p] (hrs)')
plt.legend()
#plt.title(temperature_key + "(" + time.asctime(time.localtime(now)) + ")" ) 
#pytimber.set_xaxis_date()
plt.show()

# PLOT 3 *** Vacuum Integrals
print("Plot 3")
plt.figure(figsize=(12,6))
plt.suptitle(titletext)
plt.plot(pressureD_int, normD, 'o-', color='red', markersize=2, label='Integral D')
plt.plot(pressureC_int, normC, 'o-', color='blue', markersize=2, label='Integral C')
plt.plot(pressureB_int, normB, 'o-', color='green', markersize=2, label='Integral B')
plt.plot(pressureA_int, normA, 'o-', color='grey', markersize=2, label='Integral A')
ax=plt.gca()
ax.set_ylabel(r'Normalized pressure (mbar/p)')
ax.semilogy()
ax.set_ylim(10 ** -25, 2*10 ** -20)
ax.set_xlabel(r'Pressure integral (mbar.s)')
plt.legend()
#plt.title(temperature_key + "(" + time.asctime(time.localtime(now)) + ")" ) 
#pytimber.set_xaxis_date()
plt.show()

# PLOT #4 *** Vacuum Integrals
print("Plot 4")
fig, ax1=plt.subplots()
fig.suptitle(titletext)
ax1.plot(aligned_time, pressureD_int, 'o-', color='red', markersize=2, label='Integral D')
ax1.plot(aligned_time, pressureC_int, 'o-', color='blue', markersize=2, label='Integral C')
ax1.plot(aligned_time, pressureB_int, 'o-', color='magenta', markersize=2, label='Integral B')
ax1.plot(aligned_time, pressureA_int, 'o-', color='grey', markersize=2, label='Integral A')
ax1.set_xlabel(r'Date & (UTC) time')
pytimber.set_xaxis_date(bins=7)
ax1.set_ylabel(r'Pressure integral (mbar.s)')

ax2=ax1.twinx()
ax2.plot(aligned_time, intensity, '-g', label='Beam Intensity')
ax2.set_ylabel(r'Beam intensity (p)')
ax2.tick_params(axis='y', colors='green')
ax2.yaxis.label.set_color('green')

#plt.figure(figsize=(12,6))
#plt.plot(aligned_time, pressureD_int, 'o-', color='red', markersize=3, label='Integral D')
#plt.plot(aligned_time, pressureC_int, 'o-', color='blue', markersize=3, label='Integral C')
#plt.plot(aligned_time, pressureB_int, 'o-', color='magenta', markersize=3, label='Integral B')
#plt.plot(aligned_time, pressureA_int, 'o-', color='grey', markersize=3, label='Integral A')
#ax=plt.gca()
#ax.set_ylabel(r'Pressure integral (mbar.s)')
#ax.set_xlabel(r'Date & (UTC) time')
#ax.set.twinx()


#plt.legend()
#plt.title(temperature_key + "(" + time.asctime(time.localtime(now)) + ")" ) 
#pytimber.set_xaxis_date()
plt.show()

print("Plot 5")
fig, ax1=plt.subplots(figsize=(12,6))
fig.suptitle(titletext)
ax1.plot(aligned_time, beam_elap, 'o-', color='red', markersize=2, label='Integral D')
ax1.set_xlabel(r'Date & (UTC) time')
pytimber.set_xaxis_date(bins=7)
ax1.set_ylabel(r'Elapsed beam time [>10^12 p] (hrs)')
plt.legend()

ax2=ax1.twinx()
ax2.plot(aligned_time, intensity, '-g', label='Beam Intensity')
ax2.set_ylabel(r'Beam intensity (p)')
ax2.tick_params(axis='y', colors='green')
ax2.yaxis.label.set_color('green')
plt.legend()


plt.show()